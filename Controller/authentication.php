<?php
session_start();
require 'Model/User.php';

function login(){
    $location = 'Location: index.php?action=chat';
    if (isset($_SESSION['user'])){
        header($location);
    }
    $user = new User();
    if (isset($_POST['username']) && isset($_POST['password'])) {
        $username   = htmlspecialchars($_POST['username']);
        $password = htmlspecialchars($_POST['password']);
        if ($user->connect($username, $password)) {
            $_SESSION['user']  = $username;
            header($location);
        } else {
            print "Authentication failed";
        }
    }


    require 'View/login.php';
}
