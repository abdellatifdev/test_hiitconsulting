<?php
session_start();
require 'Model/Message.php';
require 'Model/User.php';
function newMessage(){
        if (!isset($_SESSION['user'])){
            header('Location: index.php?action=login');
        }
        $chat = new Message();
        $user = new User();
        if (isset($_POST['message'])) {
            $message = htmlspecialchars($_POST['message']);
            $chat->addMessage($message,$_SESSION['user'],$_POST['recipient']);
        }
        $messages = $chat->getMessages();
        $users = $user->getUser($_SESSION['user']);
    require 'View/message.php';

}