<?php
session_start();
require 'Model/Message.php';
require 'Model/User.php';

if (isset($_SESSION['user'])) {
    $chat = new Message();
    $user = new User();
    if (isset($_POST['message'])) {
        $username  = $_SESSION['user'];
        $message = htmlspecialchars($_POST['message']);
        $chat->addMessage($message,$username,$_POST['recipient']);
    }

    $messages = $chat->getMessages();
    $users    = $user->getUser( $_SESSION['user']);

    $result             = [];
    $result['messages'] = $messages;
    $result['users']    = $users;
    echo json_encode($result);
}

