<?php
session_start();
require 'Model/User.php';

function register(){

    $location = 'Location: index.php?action=chat';
    if (isset($_SESSION['user'])){
        header($location);
    }
    if ($_SERVER['REQUEST_METHOD'] === 'POST'){
        if (isset($_POST['password']) && isset($_POST['confirmation_password']) && $_POST['password'] == $_POST['confirmation_password']) {
            if (isset($_POST['username'])) {
                $username = htmlspecialchars($_POST['username']);
                $password = htmlspecialchars($_POST['password']);
                $user = new User();
                $hashed_password = password_hash($password, PASSWORD_DEFAULT);
                $user->newUser($username, $hashed_password);
                $_SESSION['user']  = $username;
                header($location);
            } else {
                print 'Username already exists';
            }
        }
        else{
            print "Your password and confirmation password do not match.";
        }
    }
    require 'View/register.php';
}
