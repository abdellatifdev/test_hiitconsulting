<?php
/**
 * Created by PhpStorm.
 * User: Unknown
 * Date: 25/10/2019
 * Time: 23:24
 */

class Message
{
    private $db;

    function __construct()
    {
        try {
            $this->db = new PDO('mysql:host=localhost;dbname=chat', 'root', '', [
                    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                ]
            );
            $this->db->exec('SET CHARACTER SET utf8');
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    function getMessages()
    {
        $messages = [];
        $query    = $this->db->query('SELECT m.content, DATE_FORMAT(m.created_at, "%d/%m/%Y %H:%i:%s") 
        AS messageDate, m.sender , m.recipient,s.username,r.username
        FROM Message m 
        LEFT JOIN User s ON s.id = m.sender
        LEFT JOIN User r ON r.id  = m.recipient 
        ORDER BY m.id DESC LIMIT 30'
        );
        $i        = 0;
        while ($data = $query->fetch()) {
            $messages[$i] = $data;
            $i++;
        }
        return $messages;
    }

    function addMessage($content,$sender,$recipient)
    {
        $senderQuery       = $this->db->query("SELECT id from User WHERE username = '$sender'");
        $recipientQuery    = $this->db->query("SELECT id from User WHERE username = '$recipient'");
        $userSender    = $senderQuery->fetch()[0];
        $userRecipient = $recipientQuery->fetch()[0];
        if($userSender && $userRecipient){
            $query = $this->db->prepare('INSERT INTO Message(sender,content,recipient,created_at) VALUES(:sender,:content,:recipient,NOW())');
            $query->execute([
                'sender'  =>  $userSender,
                'content' => htmlspecialchars($content),
                'recipient' => $userRecipient
            ]);
        }
    }
}