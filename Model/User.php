<?php
/**
 * Created by PhpStorm.
 * User: Unknown
 * Date: 25/10/2019
 * Time: 19:12
 */

class User
{
    private $db;
    private $is_connected;
    function __construct()
    {
        // connexion avec la base de données
        try {
            $this->db = new PDO('mysql:host=localhost;dbname=chat', 'root', '', [
                    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                ]
            );
            $this->db->exec('SET CHARACTER SET utf8');
            $this->is_connected = false;
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    /*
     * Authentification
     */
    public function connect($username, $password)
    {
        $query = $this->db->query("SELECT password FROM user WHERE username = '$username'");
        $existPass  = $query->fetch()[0];
        if (password_verify($password,$existPass)) {
            $this->is_connected = true;
            $this->setStatus($username, true);
        }
        return $this->is_connected;
    }

    /*
     * fonction pour modifier l'etat d'utilisateur (s'il est connecté ou pas)
     */
    public function setStatus($username, $status)
    {
        try {
            $query = $this->db->prepare('UPDATE user SET status = :status WHERE username = :username');
            $query->execute([
                    'username' => htmlspecialchars($username),
                    'status' => htmlspecialchars($status),
                ]
            );
            return true;
        } catch (Exception $e) {
            die($e->getMessage());
        }
        return false;
    }


    public function exist($username)
    {
        $query = $this->db->query("SELECT password FROM User WHERE username = '$username'");
        if ($query->fetch()[0]) {
            return true;
        }
        return false;
    }

    /*
     * fonction pour crée un nouveau utilisateur
     */
    public function newUser($username, $password)
    {
        if (!$this->exist($username)) {
            try {
                $query = $this->db->prepare('INSERT INTO user(username, password) VALUES(:username, :password)');

                $query->execute([
                        'username'   => htmlspecialchars($username),
                        'password' => htmlspecialchars($password),
                    ]
                );
                $this->setStatus($username, true);
                return true;
            } catch (Exception $e) {
                die($e->getMessage());
            }
        }
        return false;
    }

    /*
     * foonction pour récupérer les utilisateurs connectés
     */
    public function getUser($username)
    {
        $users = [];
        $query    = $this->db->query("SELECT id,username FROM user WHERE status=1 and username != '$username' ORDER BY id DESC");
        $i        = 0;
        while ($data = $query->fetch()) {
            $users[$i] = $data;
            $i++;
        }
        return $users;
    }
    public function logout($username){
        $query = $this->db->query("SELECT id FROM User WHERE username = '$username'");
        if ($query->fetch()[0]) {
            $this->setStatus($username, 0);
            return true;
        }
        return false;
    }
}