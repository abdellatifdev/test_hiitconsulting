
<?php $title = 'Chat'; ?>
<?php ob_start(); ?>
<style>
    body{
        background-color: #eef2f5;
    }
    .message-form{
        height: 550px;
        position: relative;
        overflow: hidden;
        background: #fff;
    }
    .message-form form {
        position: absolute;
        bottom: 0;
    }
    .online {
        height: 10px;
        width: 10px;
        background-color: green;
        border-radius: 50%;
        float: right;
    }
    .messages{
        position: relative;
    }
    .messages h2{
        display: inline-block;
    }
    .messages a{
        position: absolute;
        right: 0;
        bottom: 15px;
    }
    .list-group-item {
        cursor:pointer
    }
</style>
<div class="container">
    <div class="row">
        <div class="col-md-8">
            <div class="messages">
                <h2 id="list-title">Liste des messages</h2>
                <a href="index.php?action=logout" class="btn btn-danger">Se déconnecter</a>
            </div>
            <div class="message-form">
                <div class="table-message" id="list-message">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Expéditeur</th>
                                <th>Message</th>
                                <th>Déstinataire</th>
                                <th>Date</th>
                            </tr>
                        </thead>
                        <tbody id="messages">
                        <?php foreach ($messages as $message) {
                            ; ?>
                            <tr>
                                <td><?php print $message[4]; ?></td>
                                <td><?php print $message[0]; ?></td>
                                <td><?php print $message[5]; ?></td>
                                <td><?php print $message[1]; ?></td>
                            </tr>
                        <?php }; ?>
                        </tbody>
                    </table>
                </div>
                <form action="index.php?action=chat" method="post" id="chat-form" style="display:none">
                        <div class="input-group">
                            <input type="text" name="message" class="form-control" placeholder="Taper votre message">
                            <input type="hidden" name="recipient" id="recipient">
                            <span class="input-group-btn">
                                <button class="btn btn-primary" type="submit">Envoyer</button>
                            </span>
                        </div><!-- /input-group -->
                </form>
            </div>
        </div>
        <div class="col-md-4">
            <h2 id="ok">Utilisateurs connectés </h2>
            <span>Cliquer sur un utilisateur pour discuter</span>
            <div class="message-form">
                <ul class="list-group" id="users-online">
                    <?php foreach ($users as $user) {
                        ; ?>
                        <li class="list-group-item" id="<?= $user["username"]; ?>">
                            <i class="online"></i>
                            <?php print $user["username"]; ?>
                        </li>
                    <?php }; ?>
                </ul>
            </div>
        </div>
    </div>
</div>

<?php $content = ob_get_clean(); ?>
<?php require('template.php'); ?>