
<?php $title = 'Inscription'; ?>
<?php ob_start(); ?>
<style type="text/css">
    .login-form {
        width: 340px;
        margin: 50px auto;
    }
    .login-form form {
        margin-bottom: 15px;
        background: #f7f7f7;
        box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
        padding: 30px;
    }
    .login-form h2 {
        margin: 0 0 15px;
    }
    .form-control, .btn {
        min-height: 38px;
        border-radius: 2px;
    }
    .btn {
        font-size: 15px;
        font-weight: bold;
    }
</style>
<div class="login-form">
    <form action="index.php?action=register" method="post">
        <h2 class="text-center">Inscription</h2>
        <div class="form-group">
            <input type="text" name="username" class="form-control" placeholder="Username" required="required">
        </div>
        <div class="form-group">
            <input type="password" name="password" class="form-control" placeholder="Password" required="required">
        </div>
        <div class="form-group">
            <input type="password" name="confirmation_password" class="form-control" placeholder="Confirmation password" required="required">
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary btn-block">S'inscrire</button>
        </div>
    </form>
    <p class="text-center"><a href="?action=login">Se connecter</a></p>
</div>

<?php $content = ob_get_clean(); ?>

<?php require('template.php'); ?>