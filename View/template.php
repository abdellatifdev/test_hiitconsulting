<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="public/css/bootstrap.min.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <title><?= $title ?></title>
</head>

<body>
<?= $content ?>
    <script src="public/js/jquery.min.js"></script>
    <script src="public/js/bootstrap.min.js"></script>
    <script>

            var refreshData = null;
            $(document).on('ready',function(){
                refreshData = setInterval(refresh,3000);
            });
        
       
        $(document).on('click','.list-group-item',function() {
            clearInterval(refreshData);
            $('#list-title').empty().text('discuter avec '+ $(this).text());
            $('#list-message').empty();
            $('#chat-form').css('display','block');
            $('#recipient').val($(this).attr('id'))
            refreshData = setInterval(refresh,3000);
        });
           
        function refresh() {
            $.ajax(
                {
                    type : "POST",
                    dataType : "json",
                    url : "index.php?action=refresh",
                    success : function (data) {
                        messages = data['messages'];
                        users = data['users'];
                        var users_onlince = $("#users-online"),messagesDiv = $('#messages');
                        messagesDiv.html("");
                        $.each(messages, function (key, message) {
                            messagesDiv.append('<tr><td>'+message[4]+'</td><td>' + message[0] + '</td><td>' + message[5] + '</td><td>' + message[1] + '</td></tr>');
                        });
                        users_onlince.html("");
                        $.each(users, function (key, user) {
                            users_onlince.append('<li class="list-group-item" id="'+user["username"]+'"><i class="online"></i>' + user["username"] + '</li>');
                        });

                    }
                }
            );
        }
        
</script>
</body>
</html>