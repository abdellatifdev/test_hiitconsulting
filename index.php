<?php
if (isset($_GET['action'])) {
    if ($_GET['action'] === 'register') {
        require('Controller/registration.php');
        register();
    }
    elseif ($_GET['action'] === 'login'){
        require('Controller/authentication.php');
        login();
    }
    elseif ($_GET['action'] === 'chat'){
        require('Controller/chat.php');
        newMessage();
    }
    elseif ($_GET['action'] === 'logout'){
        require('Controller/logout.php');
        logout();
    }
    elseif ($_GET['action'] === 'refresh'){
        require('Controller/real_time.php');
    }
}
else{
    require('Controller/authentication.php');
    login();
}

